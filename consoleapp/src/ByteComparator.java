import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

public class ByteComparator {
    private static final byte[] STATIC_BYTE_ARRAY = new byte[256];
    private static final String STATIC_STRING = "RandomCipherText";

    static {
        for (int i = 0; i < STATIC_BYTE_ARRAY.length; i++) {
            STATIC_BYTE_ARRAY[i] = (byte) i;
        }
    }

    public String compare(byte[] array) {
        String own = new String(STATIC_BYTE_ARRAY, StandardCharsets.UTF_8);
        if (Arrays.equals(STATIC_BYTE_ARRAY, array)) {
            return "Byte Match";
        }
        return null;
    }

    public String compareString(String input) {
//        String input = new String(array, StandardCharsets.UTF_8);
//        String own = new String(STATIC_BYTE_ARRAY, StandardCharsets.UTF_8);
        if (input.equals(STATIC_STRING)) {
            return "Byte Match";
        }
        return null;
    }

    public static void main(String[] args) {
//        String[] test = new String[]{"RandomCipherText"};
        if (args.length != 1) {
            System.out.println("Usage: java ByteComparator <byteArray>");
            System.exit(1);
        }

        byte[] array = parseByteArray(args[0]);

        if (array.length > 256) {
            System.out.println("Error: The provided byte array must be exactly 256 bytes.");
            System.exit(1);
        }

        ByteComparator comparator = new ByteComparator();
//        String result = comparator.compare(array);
        String result2 = comparator.compareString(args[0]);

        System.out.println(Objects.requireNonNullElse(result2, "Bytes do not match."));
    }

    private static byte[] parseByteArray(String input) {
        return input.getBytes();
    }
}