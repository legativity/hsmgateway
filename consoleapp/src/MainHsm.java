import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainHsm {
    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.err.println("Usage: java -jar consoleapp.jar <data> <ip> <port>");
            System.exit(1);
        }

        String command = args[0];
        String ipAddress = args[1];
        int port = Integer.parseInt(args[2]);

        // String _r = "";
        // // Connect to Client with JavaServer
        // ServerSocket serverSocket = new ServerSocket(10567);
        // // System.out.println("Server listening on port 10567...");

        // Socket clientSocket = serverSocket.accept();
        // PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        // BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        // String inputLine;
        // while ((inputLine = in.readLine()) != null) {
        //     // Connect to HSM funtion
        //     _r = hsmConnect(inputLine);
        //     // System.out.println(inputLine);
        //     // System.out.println("Received from client: " + inputLine);
        //     out.println(_r);
        // }

        // out.close();
        // in.close();
        // clientSocket.close();
        // serverSocket.close();

        String response = hsmConnect(command, ipAddress, port);

        System.out.println(response);
    }

    public static String hsmConnect(String command, String server, int port) {
        Socket socket = null;
        String response = "";
        DataOutputStream hsmOut = null;
        DataInputStream hsmIn = null;

        try {
            socket = new Socket(server, port);

            if (socket != null) {
                hsmIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                hsmOut = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                hsmOut.writeUTF(command);
                hsmOut.flush();

                int count = hsmIn.readShort();
                byte[] bt = new byte[count];
                hsmIn.readFully(bt);
                response = new String(bt);
            }
        } catch (UnknownHostException e) {
            System.out.println("UnknownHostException");
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            System.out.println("IOException - trying to connect to server");
            e.printStackTrace();
            System.exit(1);
        } finally {
            try {
                if (hsmOut != null) hsmOut.close();
                if (hsmIn != null) hsmIn.close();
                if (socket != null) socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return response;
    }
}