import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) throws IOException {

        String _r = "";
        // Connect to Client with JavaServer
        ServerSocket serverSocket = new ServerSocket(10567);
        // System.out.println("Server listening on port 10567...");

        Socket clientSocket = serverSocket.accept();
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            // Connect to HSM funtion
            _r = hsmConnect(inputLine);
            // System.out.println(inputLine);
            // System.out.println("Received from client: " + inputLine);
            out.println(_r);
        }

        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }

    public static String hsmConnect(String s) {
        int port = 1500;
        String server = "10.22.51.177";
        Socket socket = null;
        String _r = "";
        DataOutputStream hsmOut = null;
        DataInputStream hsmIn = null;
        String command = null;
        int ERROR = 1;
        // connect to server
        try {
            socket = new Socket(server, port);
            // System.out.println("<<< Socket >>> :" + socket);
            if (socket != null) {
                // System.out.println("<<< Connected to HSM  >>>:"
                //         + socket.isConnected());
                hsmIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                hsmOut = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                command = s;
                // available stream to be read
                hsmOut.writeUTF(command);
                // System.out.println("Input to HSM : " + command);
                hsmOut.flush();
                // String response = in.readUTF();//not work
                int count = hsmIn.readShort();
                byte[] bt = new byte[count];
                hsmIn.readFully(bt);
                // String response = new String(bt);
                _r = new String(bt);
                // System.out.println("Output from HSM : " + response);
                // System.out.println("");
            }
            // System.out.println("Connected with server " +
            //         socket.getInetAddress() +
            //         ":" + socket.getPort());
        } catch (UnknownHostException e) {
            System.out.println("UnknownHostException");
            System.out.println(e);
            System.exit(ERROR);
        } catch (IOException e) {
            System.out.println("IOException - trying to connect to server");
            System.out.println(e);
            System.exit(ERROR);
        }

        return _r;
    }
}