﻿using System.Diagnostics;

namespace HSMGateway;
class Program
{
    static async Task Main(string[] args)
    {
        while (true)
        {
            Console.WriteLine("Please enter text input (or type 'exit' to quit):");
            string userInput = Console.ReadLine();

            if (userInput.ToLower() == "exit")
            {
                break;
            }

            Console.WriteLine("Please enter the IP address:");
            string ipAddress = Console.ReadLine();

            Console.WriteLine("Please enter the port number:");
            string portInput = Console.ReadLine();
            int port;

            // Validate port input
            while (!int.TryParse(portInput, out port))
            {
                Console.WriteLine("Invalid port number. Please enter a valid port number:");
                portInput = Console.ReadLine();
            }

            await ExecuteJavaJar(userInput, ipAddress, port);
        }
    }

    static async Task ExecuteJavaJar(string input, string ipAddress, int port)
    {
        try
        {
            ProcessStartInfo start = new ProcessStartInfo
            {
                FileName = "java",
                Arguments = $"-jar consoleapp.jar \"{input}\" \"{ipAddress}\" {port}",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            // Start the process
            using (Process process = Process.Start(start))
            {
                // Read the output from the Java application
                string hsmResponse = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();
                process.WaitForExit();

                // Output the result to the console
                Console.WriteLine("Output from Java application:");
                Console.WriteLine(hsmResponse);

                // Check for errors
                var decrypted = "";
                if (string.IsNullOrEmpty(error))
                {
                    Decryptor decryptor = new Decryptor();
                    var model = decryptor.ExtractCipher(hsmResponse);
                    decrypted = decryptor.DecryptCardNo(model);
                }
                await ProcessCipherTextAsync(decrypted);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("An error occurred while executing the Java application:");
            Console.WriteLine(ex.Message);
            await ProcessCipherTextAsync(ex.Message);
        }
    }

    static async Task ProcessCipherTextAsync(string cipherText)
    {
        await File.WriteAllTextAsync("response.txt", cipherText);
    }
}