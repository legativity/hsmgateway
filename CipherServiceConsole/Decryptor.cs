using System.Security.Cryptography;
using System.Text;

namespace HSMGateway;
public class Decryptor
{
    private byte[] KEY = Convert.FromHexString("0E08D3E3E30248BC1E6323512D9E65AA53B97A8C30448543A47A59AB74E9EAD2");

    private string DecodeString(string base64EncodedData) 
    {
        var base64Bytes = Convert.FromBase64String(base64EncodedData);
        string hexString = BitConverter.ToString(base64Bytes).Replace("-", "");
        if (hexString.Length < 24 + 32 + 32)
        {
            throw new ArgumentException("The decoded byte array is too short.");
        }
        return hexString;
    }

    public GcmModel ExtractCipher(string encodedCipherText)
    {
        var decodedBytes = DecodeString(encodedCipherText);

        string ivHex = decodedBytes.Substring(0, 24);
        string cipherTextHex = decodedBytes.Substring(24, 32);
        string tagHex = decodedBytes.Substring(24 + 32, 32);

        return new GcmModel
        {
            IV = Convert.FromHexString(ivHex),
            CipherText = Convert.FromHexString(cipherTextHex),
            Tag = Convert.FromHexString(tagHex)
        };
    }

    public string DecryptCardNo(GcmModel hsmModel)
    {
        AesGcm aesGcm = new AesGcm(KEY);
        byte[] decryptedData = new byte[hsmModel.CipherText.Length];
        aesGcm.Decrypt(hsmModel.IV, hsmModel.CipherText, hsmModel.Tag, decryptedData);

        return Encoding.UTF8.GetString(decryptedData);
    }
}