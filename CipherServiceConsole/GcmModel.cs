namespace HSMGateway
{
  public class GcmModel {
    public byte[] IV { get; set; }

    public byte[] CipherText { get; set; }

    public byte[] Tag { get; set; }
  }
}