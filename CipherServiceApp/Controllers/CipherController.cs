using CipherServiceApp.Entities;
using CipherServiceApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace CipherServiceApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CipherController : ControllerBase
    {
        private readonly ICipherService _cipherService;

        public CipherController(ICipherService cipherService)
        {
            _cipherService = cipherService;
        }

        [HttpGet("trigger")]
        public async Task<IActionResult> Trigger([FromQuery] string cipherText)
        {
            try
            {
                string result = await _cipherService.ProcessCipherTextAsync(cipherText);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}