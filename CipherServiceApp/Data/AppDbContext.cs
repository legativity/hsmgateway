using Microsoft.EntityFrameworkCore;
using CipherServiceApp.Entities;

namespace CipherServiceApp.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<CipherText> CipherTexts { get; set; }
    }
}