namespace CipherServiceApp.Entities
{
    public class CipherText
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}