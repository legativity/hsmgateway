namespace CipherServiceApp.Services
{
    public interface ICipherService
    {
        Task<string> ProcessCipherTextAsync(string cipherText);
    }
}