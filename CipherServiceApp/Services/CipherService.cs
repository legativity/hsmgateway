using CipherServiceApp.Data;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace CipherServiceApp.Services
{
    public class CipherService : ICipherService
    {
        // private readonly AppDbContext _context;

        // public CipherService(AppDbContext context)
        // {
        //     _context = context;
        // }

        public CipherService()
        {
            
        }

        public async Task<string> ProcessCipherTextAsync(string cipherText)
        {
            // var cipherTextEntity = await _context.CipherTexts.FindAsync(id);
            // if (cipherTextEntity == null)
            //     throw new Exception("Cipher text not found");

            // string decryptedText = Decrypt(cipherTextEntity.Text);

            string jarResponse = CallJarFile(cipherText);

            await File.WriteAllTextAsync("response.txt", jarResponse);

            return jarResponse;
        }

        // private string Decrypt(string cipherText)
        // {
        //     // Todo decryption logic here
        //     return cipherText;
        // }

        private string CallJarFile(string decryptedText)
        {
            ProcessStartInfo start = new ProcessStartInfo
            {
                FileName = "java",
                Arguments = $"-jar consoleapp.jar \"{decryptedText}\"",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            using Process process = Process.Start(start);
            using StreamReader reader = process.StandardOutput;
            return reader.ReadToEnd();
        }
    }
}